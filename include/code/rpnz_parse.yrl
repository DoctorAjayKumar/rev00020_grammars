% this is like a stack collapser
% rpn is binary isn't it

% Terminals are the parts of speech
Terminals int op.

% Nonterminals are the nodes higher up in the tree
Nonterminals
    arg
    int_leaf
    op_leaf
    sentence
    .

% Root symbol is the type of node at the top of the tree
Rootsymbol sentence.


% a sentence can be just a plain operator, or a list of ints followed
% by an operator, or arguments followed by an operator
% sentence -> arg arg op_leaf : {s, '$1', '$2', '$3'}.
sentence -> arg arg op_leaf : collapse('$1', '$2', '$3').

arg -> int_leaf : '$1'.
arg -> sentence : '$1'.

int_leaf -> int : get_int('$1').
op_leaf  -> op  : get_op('$1').


Erlang code.

get_int({int, _Linum, Int}) ->
    Int.

get_op({op, _Linum, Op}) ->
    Op.

collapse(IntL, IntR, '+') ->
    IntL + IntR;
collapse(IntL, IntR, '*') ->
    IntL * IntR;
collapse(IntL, IntR, '-') ->
    IntL - IntR;
collapse(IntL, IntR, '/') ->
    IntL div IntR;
collapse(IntL, IntR, '%') ->
    IntL rem IntR.
