% File: complex4_parse.yrl

% nonterminals are our stems
Nonterminals    sentence
                determiner_phrase
                noun_phrase
                prep_phrase
                superlative_phrase
                verb_phrase
                adjective_leaf
                adverb_leaf
                conjunction_leaf
                determiner_leaf
                noun_leaf
                possessive_particle_leaf
                preposition_leaf
                superlative_leaf
                verb_leaf.

% terminals are our "types of leafs"
Terminals   adjective
            adverb
            conjunction
            determiner
            noun
            possessive_particle
            preposition
            superlative
            verb.

% what is the root type of stem?
Rootsymbol sentence.

% top level
sentence -> noun_phrase verb_phrase
          : pf_tree({stem, sentence, ['$1', '$2']}).


% possible consturctions for noun phrases
% for "the founder's humility"
noun_phrase ->  determiner_phrase
                noun_leaf
             :  {stem, noun_phrase, ['$1', '$2']}.
noun_phrase -> noun_leaf
             : {stem, noun_phrase, ['$1']}.
noun_phrase -> determiner_leaf noun_leaf
             : {stem, noun_phrase, ['$1', '$2']}.
noun_phrase -> determiner_leaf
               adjective_leaf
               noun_leaf
               prep_phrase
             : {stem, noun_phrase, ['$1', '$2', '$3', '$4']}.
% for "the smartest mathematician who ever lived"
noun_phrase ->  superlative_phrase
                noun_leaf
                prep_phrase
             :  {stem, noun_phrase, ['$1', '$2', '$3']}.

% "determiner phrase" which is a contrived category that I made up
% maybe should be called a possessive phrase but this is all fake so
% who cares
%
% interestingly, using the rule I put in the document, the parser
% contains a potential ambiguity
determiner_phrase -> noun_phrase possessive_particle_leaf
                   : {stem, determiner_phrase, ['$1', '$2']}.
% seemingly unambiguous rule:
%determiner_phrase -> determiner_leaf noun_leaf possessive_particle_leaf
%                   : {stem, determiner_phrase, ['$1', '$2']}.

prep_phrase -> preposition_leaf noun_phrase
             : {stem, prep_phrase, ['$1', '$2']}.
% now our preposition can take a verb phrase
prep_phrase -> preposition_leaf verb_phrase
             : {stem, prep_phrase, ['$1', '$2']}.

% "the smartest"
superlative_phrase -> determiner_leaf superlative_leaf
                    : {stem, superlative_phrase, ['$1', '$2']}.

% possible consturctions for verb phrases
verb_phrase -> verb_leaf
             : {stem, verb_phrase, ['$1']}.
verb_phrase -> verb_leaf noun_phrase
             : {stem, verb_phrase, ['$1', '$2']}.
verb_phrase -> adverb_leaf verb_leaf
             : {stem, verb_phrase, ['$1', '$2']}.
% new one: "admire and praise"
verb_phrase -> verb_leaf
               conjunction_leaf
               verb_leaf
               noun_phrase
             : {stem, verb_phrase, ['$1', '$2', '$3', '$4']}.

% wrap the leaf constructions in their own rule to make the stem
% rules cleaner
adjective_leaf    -> adjective   : {leaf, '$1'}.
adverb_leaf       -> adverb      : {leaf, '$1'}.
conjunction_leaf  -> conjunction : {leaf, '$1'}.
determiner_leaf   -> determiner  : {leaf, '$1'}.
noun_leaf         -> noun        : {leaf, '$1'}.
possessive_particle_leaf ->
    possessive_particle : {leaf, '$1'}.
preposition_leaf  -> preposition : {leaf, '$1'}.
superlative_leaf  -> superlative : {leaf, '$1'}.
verb_leaf         -> verb        : {leaf, '$1'}.

Erlang code.

% pretty format the whole tree (generate a deeplist of chars which we
% will flatten out in the shell)

% If it's a stem, make a sexp (StemType SubTree1 SubTree2 SubTree3 ...)
%
% The pf_subtrees/1 function handles putting spaces between the
% different trees
pf_tree({stem, StemType, SubTrees}) ->
    [$(, erlang:atom_to_list(StemType), pf_subtrees(SubTrees), $)];
pf_tree({leaf, {PartOfSpeech, _LineNumber, Word}}) ->
    [$(, erlang:atom_to_list(PartOfSpeech), " ", $", Word, $", $)].

% pretty format a list of subtrees, and handle putting spaces before
% them
pf_subtrees([]) ->
    "";
pf_subtrees([Tree | Rest]) ->
    [" ", pf_tree(Tree), pf_subtrees(Rest)].
