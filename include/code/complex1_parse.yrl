% File: complex1_parse.yrl

% nonterminals are our stems
Nonterminals sentence noun_phrase verb_phrase noun_leaf verb_leaf determiner_leaf.

% terminals are our "types of leafs"
Terminals noun verb determiner.

% what is the root type of stem?
Rootsymbol sentence.

% top level
sentence    -> noun_phrase verb_phrase      : pf_tree({stem, sentence, ['$1', '$2']}).

% possible consturctions for noun phrases
noun_phrase -> noun_leaf                    : {stem, noun_phrase, ['$1']}.
noun_phrase -> determiner_leaf noun_leaf    : {stem, noun_phrase, ['$1', '$2']}.

% possible consturctions for verb phrases
verb_phrase -> verb_leaf                    : {stem, verb_phrase, ['$1']}.
verb_phrase -> verb_leaf noun_phrase        : {stem, verb_phrase, ['$1', '$2']}.

% wrap the leaf constructions in their own rule to make the stem
% rules cleaner
determiner_leaf   -> determiner : {leaf, '$1'}.
noun_leaf         -> noun       : {leaf, '$1'}.
verb_leaf         -> verb       : {leaf, '$1'}.

Erlang code.

% pretty format the whole tree (generate a deeplist of chars which we
% will flatten out in the shell)

% If it's a stem, make a sexp (StemType SubTree1 SubTree2 SubTree3 ...)
%
% The pf_subtrees/1 function handles putting spaces between the
% different trees
pf_tree({stem, StemType, SubTrees}) ->
    [$(, erlang:atom_to_list(StemType), pf_subtrees(SubTrees), $)];
pf_tree({leaf, {PartOfSpeech, _LineNumber, Word}}) ->
    [$(, erlang:atom_to_list(PartOfSpeech), " ", $", Word, $", $)].

% pretty format a list of subtrees, and handle putting spaces before
% them
pf_subtrees([]) ->
    "";
pf_subtrees([Tree | Rest]) ->
    [" ", pf_tree(Tree), pf_subtrees(Rest)].
