% File: complex3_lex.xrl

Definitions.

WHITESPACE = [\s\t\n\r]

Rules.

admire          : {token, {verb, TokenLine, TokenChars}}.
divine          : {token, {adjective, TokenLine, TokenChars}}.
intellect       : {token, {noun, TokenLine, TokenChars}}.
everybody       : {token, {noun, TokenLine, TokenChars}}.
founder         : {token, {noun, TokenLine, TokenChars}}.
ever            : {token, {adverb, TokenLine, TokenChars}}.
he              : {token, {noun, TokenLine, TokenChars}}.
i               : {token, {noun, TokenLine, TokenChars}}.
is              : {token, {verb, TokenLine, TokenChars}}.
know            : {token, {verb, TokenLine, TokenChars}}.
lived           : {token, {verb, TokenLine, TokenChars}}.
love            : {token, {verb, TokenLine, TokenChars}}.
mathematician   : {token, {noun, TokenLine, TokenChars}}.
of              : {token, {preposition, TokenLine, TokenChars}}.
poops           : {token, {verb, TokenLine, TokenChars}}.
the             : {token, {determiner, TokenLine, TokenChars}}.
smartest        : {token, {superlative, TokenLine, TokenChars}}.
who             : {token, {preposition, TokenLine, TokenChars}}.

{WHITESPACE}+   : skip_token.

Erlang code.
