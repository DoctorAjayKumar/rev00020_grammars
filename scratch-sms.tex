\section{Aside: signed multisets}

In other work, \TheFounder\ has begun to use a data structure called
a \term{signed multiset}. A \term{multiset} is a set that keeps track
of how many times an element occurs.

\begin{itemize}
\item
    The \term{sets} \lstinline|(set 1 2 3)| and
    \lstinline|(set 1 2 2 3)|
    are \textbf{not} distinct, because they contain exactly the
    same elements.
\item
    The \term{multisets} \lstinline|(ms 1 2 3)| and
    \lstinline|(ms 1 2 2 3)|
    \textbf{are} distinct, because the element
    \lstinline|2| occurs twice in the latter but only once in the
    former.
\end{itemize}

The number of times an element occurs in a multiset is called the
\term{occurance} of that element.

With multisets, the natural composition operation is addition:
\emph{the occurance of an element in a sum is the sum of its
occurances in the summands}.
More concretely, you can think of this as concatenating the two lists
and then sorting. In pseudocode:

\begin{lstlisting}
(= (union (set 1 2) (set 2 3))
   (set 1 2 3))
(= (+ (ms 1 2) (ms 2 3))
   (ms 1 2 2 3))
\end{lstlisting}

\term{Signed multisets} simply extend this idea by introducing
additive inverses: an element can occur 0 times, once, more than
once, or a negative number of times.

An application of signed multisets is the concept of ``dimensions''
or ``units'': quantities that have units can be thought of as a tuple
\lstinline|{unitted, Quantity, Unit}|. Multiplying two unitted
quantities corresponds to taking the \textbf{product} of the
quantities and \textbf{sum} of the units. As an illustration:

\begin{lstlisting}
1> MetersPerSecond = qx_sms:collapse([{m, 1}, {s, -1}]).
[{m,1},{s,-1}]
2> Seconds = qx_sms:singleton(s).
[{s,1}]
3> Meters = qx_sms:plus(MetersPerSecond, Seconds).
[{m,1}]
\end{lstlisting}

That Erlang module implementing signed multisets can be found at
\url{https://gitlab.com/DoctorAjayKumar/qanal/-/blob/1cb2a915563c7916729aec858db21e87987056dc/src/qx_sms.erl}.

The \term{product} of two multisets is similarly well-defined:
\emph{the occurance of an element in the product is the product of
its occurances in the multiplicands}. This nicely extends the concept
of ``intersection'' found in set theory.

\begin{lstlisting}
(= (* (ms 1 2) (ms 2 3))
   (ms 2))
(= (* (ms 1 2 2) (ms 2 3))
   (ms 2 2))
\end{lstlisting}

\begin{lstlisting}
8> Meters.
[{m,1}]
9> Seconds.
[{s,1}]
10> MetersPerSecond.
[{m,1},{s,-1}]
11> qx_sms:times(Meters, Seconds).
[]
12> qx_sms:times(Meters, MetersPerSecond).
[{m,1}]
13> qx_sms:times(Seconds, MetersPerSecond).
[{s,-1}]
14> OneTwoTwo = qx_sms:from_list([1,2,2]).
[{1,1},{2,2}]
15> TwoThree = qx_sms:from_list([2,3]).
[{2,1},{3,1}]
16> qx_sms:plus(OneTwoTwo, TwoThree).
[{1,1},{2,3},{3,1}]
17> qx_sms:times(OneTwoTwo, TwoThree).
[{2,2}]
\end{lstlisting}


\TheFounder\ currently thinks that the proper way to think of
traditional set theory is as ``signed multiset theory mod 2.''
WF Algebra makes this point concrete.

What meaning can signed multiset theory be given in other modulos?

There is no reason the occurances must strictly be integers. Natural,
integer, rational, dihedral, or quaternion multisets are just as
well-defined.
